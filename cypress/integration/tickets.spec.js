
describe("tickets", () =>{
    beforeEach(() => cy.visit("https://ticket-box.s3.eu-central-1.amazonaws.com/index.html"));
    
    it("fills all the text input fields", () => {

        cy.get("#first-name").type("bruno");
        cy.get("#last-name").type("lause");
        cy.get("#email").type("bruno@gmail.com");
        cy.get("#requests").type("simple text");
        cy.get("#signature").type("bruno lause");
    });

    it("select two tickets", () => {

        cy.get("#ticket-quantity").select("2");
    });

    it("select vip", () => {

        cy.get("#vip").check();
    });

    it("select checkbox", () => {

        cy.get("#friend").check();
        cy.get("#publication").check();
        cy.get("#friend").uncheck();
    });

    it("has 'TICKETBOX' header's heading", () => {
        cy.get("header h1").should("contain", "TICKETBOX");
    });

    it("invalid email", () => {
        cy.get("#email")
            .as("email")
            .type("bruno-gmail.com");

        cy.get("#email.invalid").should("exist");

        cy.get("@email")
            .clear()
            .type("dksodka@gmail.com");

        cy.get("#email.invalid").should("not.exist");
    })

    it("preenche de reseta campos", () => {
        const firstName = "bruno";
        const lastName = "lause";
        const fullName = `${firstName} ${lastName}`;
        
        cy.get("#first-name").type(firstName);
        cy.get("#last-name").type(lastName);
        cy.get("#email").type("bruno@gmail.com");
        cy.get("#requests").type("simple text");
        cy.get("#signature").type(fullName);
        cy.get("#ticket-quantity").select("2");
        cy.get("#vip").check();
        cy.get("#friend").check();
        cy.get("#publication").check();

        cy.get(".agreement p").should(
            "contain",
            `I, ${fullName}, wish to buy 2 VIP tickets`);
        
        cy.get("#agree").check();

        cy.get("button[type='submit']")
            .as("buttonSubmit")
            .should("not.be.disabled");

        cy.get(".reset").click();
        
        cy.get("@buttonSubmit")
            .should("be.disabled");

        
            cy.get(".agreement p").should(
                "contain",
                `I, , wish to buy 1 General Admission ticket`);
    });


    it("preeche campos com commands", () => {
        const costumer = {
            firstName: "bruno",
            lastName: "lause",
            email: "bruno@gmail.com"
        }

        cy.preencherDados(costumer);

        cy.get("button[type='submit']")
            .as("buttonSubmit")
            .should("not.be.disabled");

            cy.get("#agree").uncheck();
        
        cy.get("@buttonSubmit")
            .should("be.disabled");
    });
});